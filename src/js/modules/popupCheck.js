function popupCheck() {
    const popupInput = document.querySelectorAll('.Checkbox__input');
    const popupFooterBtn = document.querySelectorAll(
        '.popup-interior-door__footer'
    );

    try {
        console.log('popupCheck');

        window.addEventListener('click', (event) => {
            if (event.target.closest('.pick-up-classic')) {
                let parentLi = event.target.closest('LI');
                if (parentLi && parentLi.querySelector('input').checked) {
                    parentLi.classList.add('popup-interior-door__item--active');
                } else {
                    parentLi.classList.remove('popup-interior-door__item--active');
                    popupCounter();
                }
                let eventInput = event.target
                    .closest('.pick-up-classic')
                    .querySelectorAll('.Checkbox__input');

                function check() {
                    eventInput.forEach((chek, sum) => {
                        if (eventInput[sum].checked) {
                            popupFooterBtn[0]
                                .querySelector('button')
                                .classList.remove('popup__disabled');
                            popupCounter();
                            chek.preventDefault();
                        } else {
                            popupFooterBtn[0]
                                .querySelector('button')
                                .classList.add('popup__disabled');
                        }
                    });
                }
                check();
            } else if (event.target.closest('.pick-up-molding')) {
                let parentLi = event.target.closest('LI');
                if (parentLi && parentLi.querySelector('input').checked) {
                    parentLi.classList.add('popup-interior-door__item--active');
                } else {
                    parentLi.classList.remove('popup-interior-door__item--active');
                    popupCounter();
                }
                let eventMou = event.target
                    .closest('.pick-up-molding')
                    .querySelectorAll('.Checkbox__input');

                function checdk() {
                    eventMou.forEach((chekw, suwm) => {
                        if (eventMou[suwm].checked) {
                            popupFooterBtn[1]
                                .querySelectorAll('button')[1]
                                .classList.remove('popup__disabled');
                            popupCounter();
                            chekw.preventDefault();
                        } else {
                            popupFooterBtn[1]
                                .querySelectorAll('button')[1]
                                .classList.add('popup__disabled');
                        }
                    });
                }
                checdk();
            } else {
                // console.log('Unknown');
            }
        });
    } catch {
        console.log('popupCheck with error!!!');
    }
}