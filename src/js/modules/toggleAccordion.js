// const clickChild = document.querySelectorAll('.child-click')
const elem = document.querySelectorAll('.parent-click');
const elmChild = document.querySelectorAll('.child-click');

function toggleAccordion() {
    try {
        console.log('toggleAccordion')
        $('.parent-click').on('click', function(e) {
            $(this).find('.child-click').slideToggle(600);
        })

        window.addEventListener('scroll', (e) => {
            elem.forEach((item, index) => {
                let coord = elem[index].getBoundingClientRect();
                elmChild[index].style.left = `${coord.x - coord.width / 2}px`;
                elmChild[index].style.top = `${coord.y + coord.height}px`;
            });
        });

        window.addEventListener('resize', (e) => {
            let coord = elem.getBoundingClientRect();
            elmChild.style.left = `${coord.x - coord.width}px`;
            elmChild.style.top = `${coord.y + coord.height}px`;
        });


        // window.addEventListener('click', function(e) {
        //     let ggg = e.target.closest('.parent-click');

        //     if (ggg.closest('.child-click')) {
        //         const kkk = e.target.closest('.child-click');
        //         kkk.style.display = "block";

        //     } else {
        //         clear()
        //             // $('.child-click').each((index, item) => item.style.display = "none")
        //             // $.each($('.child-click'), function(i, value))
        //     }
        // })


    } catch (e) {
        console.log('Error toggleAccordion')
    }



}

const clear = () => {
    elmChild.forEach(item => item.style.display = "none")
}