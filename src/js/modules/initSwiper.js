function initSwiper() {
  console.log("Swiper");
  let swiperVideoRevies = new Swiper(
    ".section-video-reviews__swiper-container",
    {
      slidesPerView: 2,
      spaceBetween: 10,
      loop: true,
      navigation: {
        nextEl: ".Swiper-button-next-video-reviews",
        prevEl: ".Swiper-button-prev-video-reviews",
      },
      breakpoints: {
        730: {
          slidesPerView: 3,
          spaceBetween: 10,
          slidesPerGroup: 1,
        },
        991: {
          slidesPerView: 4,
          spaceBetween: 10,
          slidesPerGroup: 1,
        },
      },
    }
  );

  let swiperImgCatalog = new Swiper(".section-catalog__swiper-container", {
    slidesPerView: 1,
    spaceBetween: 10,
    loop: true,
    navigation: {
      nextEl: ".Swiper-button-next-video-reviews",
      prevEl: ".Swiper-button-prev-video-reviews",
    },
    breakpoints: {
      1024: {
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      },
    },
  });
  let swiperLink = new Swiper(".section-directory__swiper-container", {
    // slidesPerView: 3,
		slidesPerView: "auto",
    spaceBetween: 10,
    // preventClicks: false,
    clickable: true,
    // loop: true,
    navigation: {
      nextEl: ".Swiper-button-next-video-reviews",
      prevEl: ".Swiper-button-prev-video-reviews",
    },
    breakpoints: {
      1024: {
        // spaceBetween: 10,
      },
    },
  });
  let swiperPopUpMaster = new Swiper(
    ".popup-selection-master__swiper-container, .popup-interior-door__swiper-container",
    {
      slidesPerView: 1,
      observer: true,
      autoHeight: true,
      simulateTouch: false,
      navigation: {
        nextEl: ".popup-swiper-btn-next-js",
        prevEl: ".popup-swiper-btn-prev-js",
      },
    }
  );
	let swiperPopUpInformation = new Swiper(
    ".popup-information__swiper-container",
    {
      slidesPerView: 1,
      observer: true,
      // autoHeight: true,
      simulateTouch: true,
      navigation: {
        nextEl: ".popup-swiper-btn-next-js",
        prevEl: ".popup-swiper-btn-prev-js",
      },
    }
  );

  let swiperImgCardElementary = new Swiper(
    ".item-Card-elementary__swiper-container",
    {
      slidesPerView: 1,
      spaceBetween: 10,
      loop: true,
      navigation: {
        nextEl: ".Swiper-button-next-video-reviews",
        prevEl: ".Swiper-button-prev-video-reviews",
      },
    }
  );

  let swiperSectionProducts = new Swiper(
    ".section-products__swiper-container",
    {
      slidesPerView: 2,
      spaceBetween: 12,
      loop: true,
      // autoHeight: true,
      observer: true,
      observeParents: true,
      simulateTouch: false,
      navigation: {
        nextEl: ".Swiper-button-next-video-reviews",
        prevEl: ".Swiper-button-prev-video-reviews",
      },
      breakpoints: {
        650: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
      },
    }
  );
  let swiperSectionRelatedProducts = new Swiper(
    ".section-related-products__swiper-container",
    {
      slidesPerView: 1,
      spaceBetween: 10,
      loop: true,
      // autoHeight: true,
      observer: true,
      observeParents: true,
      simulateTouch: false,
      navigation: {
        nextEl: ".Swiper-button-next-video-reviews",
        prevEl: ".Swiper-button-prev-video-reviews",
      },
      breakpoints: {
        1250: {
          slidesPerView: 4,
          spaceBetween: 20,
        },
        850: {
          slidesPerView: 3,
          spaceBetween: 12,
        },
        550: {
          slidesPerView: 2,
          spaceBetween: 12,
        },
      },
    }
  );
  let swiperComparesCards = new Swiper(
    ".compares-cards__swiper-container",
    {
      slidesPerView: 4,
			spaceBetween: 10,
			slidesPerColumn: 2,
			slidesPerColumnFill: 'row',
      // autoHeight: true,
      observer: true,
      observeParents: true,
      simulateTouch: false,


			scrollbar: {
				el: ".swiper-scrollbar",
				hide: false,
				draggable: true,
			},
      navigation: {
        nextEl: ".Swiper-button-next-video-reviews",
        prevEl: ".Swiper-button-prev-video-reviews",

      },
      breakpoints: {
        1340: {
					slidesPerView: 4,
					spaceBetween: 10,
					slidesPerColumn: 2,
					slidesPerColumnFill: 'row',
        },
				1080: {
					slidesPerView: 3,
					spaceBetween: 0,
					slidesPerColumn: 2,
					slidesPerColumnFill: 'row',
        },
        867: {
					slidesPerView: 2,
					spaceBetween: 0,
					slidesPerColumn: 2,
					slidesPerColumnFill: 'row',
        },
				200: {
					slidesPerView: 1,
					spaceBetween: 0,
					slidesPerColumn: 2,
					slidesPerColumnFill: 'row',
					pagination: {
						el: ".swiper-pagination",
						type: "fraction",
					},
        },
      },
    }
  );

  function doubleSliderSwiper() {
    try {
      let swiper, swiper2;
      const swiperButton = document.querySelectorAll(
        ".Thumbnail-slide__swiper-button"
      );

      const swiperSlides = document.querySelectorAll(
        ".Thumbnail-slide__swiper-slide[data-min-image]"
      );

      const previewSlides = Number(
        document
          .querySelector(".Thumbnail-slide__swiper[data-preview-slides]")
          .getAttribute("data-preview-slides")
      );

      let sumSlides = swiperSlides.length;

      if (sumSlides <= previewSlides) {
        swiper = new Swiper(".Thumbnail-slide-swiper", {
          slidesPerView: previewSlides,
          watchSlidesProgress: true,
          allowTouchMove: false,
          spaceBetween: 5,
          breakpoints: {
            955: {
              spaceBetween: 12,
              allowTouchMove: false,
            },
            730: {
              spaceBetween: 12,
              allowTouchMove: false,
            },
            370: {
              spaceBetween: 12,
              allowTouchMove: true,
            },
            100: {
              allowTouchMove: true,
            },
          },
        });
        swiper2 = new Swiper(".Thumbnail-slide-swiper-2", {
          spaceBetween: 10,
          thumbs: {
            swiper: swiper,
          },
        });
        swiperButton.forEach(function (itemSwiperButton) {
          itemSwiperButton.classList.add("Vb--hidden");
        });
      } else {
        swiper = new Swiper(".Thumbnail-slide-swiper", {
          loop: true,
          slidesPerView: previewSlides,
          watchSlidesProgress: true,
          spaceBetween: 5,
          navigation: {
            nextEl: ".Thumbnail-slide__swiper-button-next",
            prevEl: ".Thumbnail-slide__swiper-button-prev",
          },
          breakpoints: {
            955: {
              spaceBetween: 12,
              allowTouchMove: false,
            },
            730: {
              spaceBetween: 12,
              allowTouchMove: false,
            },
            370: {
              spaceBetween: 12,
              allowTouchMove: true,
            },
            100: {
              allowTouchMove: true,
            },
          },
        });
        swiper2 = new Swiper(".Thumbnail-slide-swiper-2", {
          loop: true,
          // spaceBetween: 10,
          thumbs: {
            swiper: swiper,
          },
        });
      }
    } catch {
      console.log("Ошибка initSwiper (Thumbnail-slide)");
    }
  }

  doubleSliderSwiper();
}
