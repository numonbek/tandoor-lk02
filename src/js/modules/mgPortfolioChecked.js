function mgPortfolioChecke() {
  const customMaskTel = document.querySelectorAll('.customMaskTel');

  // var element = document.getElementById('selector');
  var maskOptions = {
    mask: '+{7}(000)000-00-00',
  };
  // var mask = IMask(customMaskTel, maskOptions);
  customMaskTel.forEach((item) => {
    let mask = IMask(item, maskOptions);
  });
  // customMaskTel.addEventListener('input', (e) => {
  //     customMaskTel.setAttribute("placeholder", mask.value)
  // })

  const orderItem = document.querySelectorAll('.ordering-page__list-item');
  const mgNavItem = document.querySelectorAll('.mg-navigation-list__item');

  function checkInputAgent() {
    orderItem.forEach((value, index, array) => {
      const form = document.querySelectorAll('form');

      if (orderItem[0].closest('.checked')) {
        const formFilled = orderItem[0].querySelector('form');
        if (formFilled.classList.contains('form-filled')) {
          removeErrorAddSucess(orderItem, mgNavItem, 0);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 0);
        }
      }
      if (orderItem[1].closest('.checked')) {
        const formFilled = orderItem[1].querySelector('form');
        if (formFilled.classList.contains('form-filled')) {
          removeErrorAddSucess(orderItem, mgNavItem, 1);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 1);
        }
      }
      if (orderItem[2].closest('.checked')) {
        const child = orderItem[2].querySelector('.mg-table__body-container');

        if (child.children.length > 0) {
          removeErrorAddSucess(orderItem, mgNavItem, 2);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 2);
        }
      }
      if (orderItem[3].closest('.checked')) {
        const child = orderItem[3].querySelector('.mg-table__body-container');

        if (child.children.length > 0) {
          removeErrorAddSucess(orderItem, mgNavItem, 3);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 3);
        }
      }
      if (orderItem[4].closest('.checked')) {
        const child = orderItem[4].querySelector('.mg-table__body-container');

        if (child.children.length > 0) {
          removeErrorAddSucess(orderItem, mgNavItem, 4);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 4);
        }
      }
      if (orderItem[5].closest('.checked')) {
        const img = orderItem[5].querySelector('.img-area');

        if (img.classList.contains('added-img')) {
          removeErrorAddSucess(orderItem, mgNavItem, 5);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 5);
        }
      }
      if (orderItem[6].closest('.checked')) {
        const categoryContainer = orderItem[6].querySelector('.mg-add-categories__list');
        if (categoryContainer.children.length > 0) {
          removeErrorAddSucess(orderItem, mgNavItem, 6);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 6);
        }
      }
      if (orderItem[7].closest('.checked')) {
        const inp = orderItem[7].querySelectorAll('form input');

        if (
          inp[0].classList.contains('matched') &&
          inp[1].classList.contains('matched') &&
          inp[2].classList.contains('matched')
        ) {
          removeErrorAddSucess(orderItem, mgNavItem, 7);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 7);
        }
      }
      if (orderItem[8].closest('.checked')) {
        const content = orderItem[8].querySelectorAll('.attendance-photo__container');

        if (content[0].children.length > 0 && content[1].children.length > 0) {
          removeErrorAddSucess(orderItem, mgNavItem, 8);
        } else {
          removeSucessAddError(orderItem, mgNavItem, 8);
        }
      }
    });
  }

  /* hguy */
  window.addEventListener('click', (e) => {
    /* проверка инпута сработает толыко внутри  класса ordering-page-checkstatus*/
    if (!e.target.closest('.profile-agent')) return false;

    if (e.target.closest('.ordering-page__list-item')) {
      const main = e.target.closest('.ordering-page__list-item');
      checkInputAgent();
    } else {
      checkInputAgent();
      orderItem.forEach((item) => {
        item.classList.remove('ordering-page__list-item--select');
      });
    }
  });

  // check function for form
  /**
   * @param {checkValidations} content get form
   * @param {checkValidations} inputs get all inputs from content
   */
  function checkValidations(content) {
    const inputs = content.querySelectorAll('input');
    const first = inputs[0].closest('.input-validation--valid');
    const second = inputs[1].closest('.input-validation--valid');
    const third = inputs[2].closest('.input-validation--valid');
    inputs.forEach((item, index) => {
      if (first && second && third) {
        item.closest('form').classList.add('n-input-checked');
      } else {
        item.closest('form').classList.remove('n-input-checked');
      }
    });
  }

  function removeSucessAddError(orderItem, navItem, num) {
    orderItem[num].classList.remove('ordering-page__list-item--disable');
    orderItem[num].classList.remove('ordering-page__list-item--success');
    orderItem[num].classList.add('ordering-page__list-item--error');
    navItem[num].classList.remove('ordering-page__list-item--disable');
    navItem[num].classList.remove('ordering-page__list-item--success');
    navItem[num].classList.add('ordering-page__list-item--error');
  }

  function removeErrorAddSucess(orderItem, navItem, num) {
    orderItem[num].classList.remove('ordering-page__list-item--disable');
    orderItem[num].classList.remove('ordering-page__list-item--error');
    orderItem[num].classList.add('ordering-page__list-item--success');
    navItem[num].classList.remove('ordering-page__list-item--disable');
    navItem[num].classList.remove('ordering-page__list-item--error');
    navItem[num].classList.add('ordering-page__list-item--success');
  }

  function checkValid() {
    const forms = document.querySelectorAll('form');

    forms.forEach((Form, index) => {
      if (Form.closest('.mg-comments-box__container')) {
        const inp = Form.querySelectorAll('input');
        const errorMessage = Form.querySelectorAll('.input-validation__message');

        inp.forEach((item, i) => {
          try {
            item.addEventListener('input', (e) => {
              if (e.target.value == '') {
                item.style.borderColor = 'red';
                errorMessage[i].innerText = 'поля должны быть заполнено';
                errorMessage[i].style.color = 'red';
                errorMessage[i].style.display = 'flex';
                errorMessage[i].style.justifyContent = 'end';
              } else {
                errorMessage[i].innerText = '';
                item.style.borderColor = '#e3e6f3';
              }
              if (inp[0].value !== '' && inp[1].value !== '') {
                Form.classList.add('form-filled');
              } else {
                Form.classList.remove('form-filled');
              }
            });
          } catch {}
        });
      }
      if (Form.closest('.mg-contacts-box__container')) {
        const inp = Form.querySelectorAll('input');
        const errorMessage = Form.querySelectorAll('.input-validation__message');

        inp.forEach((item, i) => {
          try {
            item.addEventListener('input', (e) => {
              if (e.target.type == 'tel') {
                if (inp[i].value.length < 16) {
                  item.style.borderColor = 'red';
                  errorMessage[i].innerText = 'поля должны быть заполнено';
                  errorMessage[i].style.color = 'red';
                  errorMessage[i].style.display = 'flex';
                  errorMessage[i].style.justifyContent = 'end';
                } else {
                  errorMessage[i].innerText = '';
                  item.style.borderColor = '#e3e6f3';
                }
              }
              if (e.target.type == 'email') {
                if (inp[2].value == '') {
                  item.style.borderColor = 'red';
                  errorMessage[2].innerText = 'поля должны быть заполнено';
                  errorMessage[2].style.color = 'red';
                  errorMessage[2].style.display = 'flex';
                  errorMessage[2].style.justifyContent = 'end';
                } else {
                  errorMessage[2].innerText = '';
                  item.style.borderColor = '#e3e6f3';
                }
              }

              if (inp[0].value.length >= 16 && inp[1].value.length >= 16 && inp[2].value != '') {
                Form.classList.add('form-filled');
              } else {
                Form.classList.remove('form-filled');
              }
            });
          } catch {}
        });
      }
    });
  }
  checkValid();
}
