function initAddModifation() {
    console.log("initAddModifation");
    try {
        //! Добавление класса к одному элементу без переключения активного модификатора
        function windowClick(closest, itemclick, classs) {
            let itemClickk = document.querySelectorAll(`.${itemclick}`);

            itemClickk.forEach((item, index) => {
                item.addEventListener("mousedown", (event) => {
                    if (

                        event.target.closest(`.${closest}`) &&
                        event.target.closest(`.${itemclick}`)

                    ) {
                        itemClickk[index].classList.toggle(classs);
                    } else {
                        return false;
                    }
                });
            });
        }

        windowClick("Card-product", "Icon-like-js", "Icon-like--active");
        windowClick("Card-product", "Icon-scales-js", "Icon-scales--active");
        windowClick("Card-product", "Card-product__btn-specifications-js", "Card-product__btn-specifications--active");

        windowClick("Card-product-aflat", "Icon-like-js", "Icon-like--active");
        windowClick("Card-product-aflat", "Icon-scales-js", "Icon-scales--active");
        windowClick("Card-product-aflat", "Card-product__btn-specifications-js", "Card-product__btn-specifications--active");


        windowClick("Product-description", "Icon-like-js", "Icon-like--active");
        windowClick("Product-description", "Icon-scales-js", "Icon-scales--active");

        windowClick("item-Card-buy", "item-Card-buy__amount-btn", "item-Card-buy--active");

        //! Открытие по кнопки , других кнопок 

        function toggleAllSpecification(elem, elements, btns) {
            const mainElem = document.querySelector(elem)
            const mainElements = document.querySelectorAll(elements)
            if (!mainElem) return
            mainElem.addEventListener('click', () => {
                mainElem.classList.toggle('btn-specifications-js--active')
                if (mainElem.closest(".btn-specifications-js--active")) {
                    mainElements.forEach((item) => {
                        item.classList.add('Card-product--active')
                        item.querySelector(btns).classList.add('Card-product__btn-specifications--active')
                    })
                } else {
                    mainElements.forEach((item) => {
                        item.classList.remove('Card-product--active')
                        item.querySelector(btns).classList.remove('Card-product__btn-specifications--active')
                    })
                }
            })

        }

        toggleAllSpecification('.btn-specifications-js', '.Card-product', '.Card-product__btn-specifications-js')


        //! Добавление класса к элементу При наведении с автоматическим присвоением активного модификатора, к аналогичному элементу
        // function toggleHoverClass(main, elem, classActive) {
        // 	document.addEventListener("mouseover", (event) => {
        // 		if (event.target.closest(main) && event.target.closest(elem)) {
        // 			const mainPath = event.target.closest(main);
        // 			const mainElement = event.target.closest(elem);
        // 			const elements = mainPath.querySelectorAll(elem);

        // 			elements.forEach((item) => {
        // 				item.classList.remove(classActive)
        // 			});
        // 			mainElement.classList.add(classActive);
        // 		}
        // 		else {
        // 			document.addEventListener("mouseout", (event) => {
        // 				const mainElement = event.target.closest(elem);
        // 				mainElement.classList.remove(classActive);
        // 			});
        // 		}
        // 	});
        // }

        // toggleHoverClass(".right-navbar", ".right-navbar__item", "right-navbar__item--active");

        //! Добавление класса к элементу с автоматическим присвоением активного модификатора, к аналогичному элементу
        function toggleClass(main, elem, classActive) {
            document.addEventListener("click", (event) => {
                if (event.target.closest(main) && event.target.closest(elem)) {
                    const mainPath = event.target.closest(main);
                    const mainElement = event.target.closest(elem);
                    const elements = mainPath.querySelectorAll(elem);

                    elements.forEach((item) => {
                        item.classList.remove(classActive)
                    });
                    mainElement.classList.add(classActive);
                }
            });
        }

        toggleClass(".Card-product", ".Palette__item-color-selection-js", "Palette__item-color-selection--active");
        toggleClass(".Card-product-aflat", ".Palette__item-color-selection-js", "Palette__item-color-selection--active");
        toggleClass(".Palette__color-selection", ".Palette__item-color-selection-js", "Palette__item-color-selection--active");
        toggleClass(".Anchor-products", ".Anchor-products__nav-item", "Anchor-products__nav-item--active");
        toggleClass(".ordering-page__delivery", ".delivery", "delivery--select");
        toggleClass(".ordering-page__payment", ".payment", "payment--select");

        toggleClass(".ordering-page__comments", ".comments", "comments--select");



        function toggleLink(elem, classActive) {
            document.addEventListener("click", (event) => {
                if (event.target.closest(elem)) {
                    const mainPath = event.target.closest(elem)
                    const elements = document.querySelectorAll(elem)
                    elements.forEach((item) => {
                        item.classList.remove(classActive)
                    });
                    mainPath.classList.add(classActive)
                }
            });
        }

        toggleLink(".section-directory__slide", "section-directory__slide--active");



        //! Добавление класса к нескольким элементам
        function addClass(main, elem, classActiveMain, classActiveElem) {
            document.addEventListener("mousedown", (event) => {
                if (event.target.closest(main) && event.target.closest(elem)) {
                    const mainPath = event.target.closest(main);
                    const mainElement = mainPath.querySelector(elem);

                    mainPath.classList.toggle(classActiveMain);
                    mainElement.classList.toggle(classActiveElem);
                }
            });
        }
        addClass(".item-Card-buy", ".item-Card-buy__amount-btn", "item-Card-buy--active", "item-Card-buy__amount-btn--active");



        //! Добавление и удаление Svg в спрайт (замена одного на другого по клику)
        function toggleSvg(mainElem, elem, oldName, newName, addSvg) {
            document.addEventListener("mousedown", (event) => {
                if (event.target.closest(mainElem) && event.target.closest(elem)) {
                    const mainPath = event.target.closest(mainElem);
                    const mainElement = mainPath.querySelector(elem);
                    const mainSvg = mainElement.querySelector(addSvg);



                    if (!mainElement.hasAttribute("data-toggle-svg")) {
                        mainElement.setAttribute("data-toggle-svg", "");
                        mainSvg.setAttribute("xlink:href", newName);

                    } else {
                        mainElement.removeAttribute("data-toggle-svg");
                        mainSvg.setAttribute("xlink:href", oldName);
                    }
                }
            });
        }

        toggleSvg(".Card-product", ".Icon-like-js", "#like", "#like-full", ".Icon__svg-use");
        toggleSvg(".Card-product", ".Icon-like-mobile-js", "#like", "#like-full", ".Icon__svg-use");
        toggleSvg(".Card-product-aflat", ".Icon-like-js", "#like", "#like-full", ".Icon__svg-use");
        toggleSvg(".Product-description", ".Icon-like-js", "#like", "#like-full", ".Icon__svg-use");


    } catch (e) {
        console.log("Ошибка toggleSvg", e);
    }
}