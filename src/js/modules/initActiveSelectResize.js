function initActiveSelectResize() {
  console.log("initActiveSelectResize");
  try {
		const btnColThree = document.querySelector('.section-directory__tab[data-index-tab="1"]')
		const btnRowTwo = document.querySelector('.section-directory__tab[data-index-tab="2"]')
		const btnColTwo = document.querySelector('.section-directory__tab[data-index-tab="3"]')
		const btnRowColOne = document.querySelector('.section-directory__tab[data-index-tab="4"]')
		const btnAll = document.querySelectorAll('.section-directory__tab[data-index-tab]')

		const tabContentRowTwo = document.querySelector('.Tabs__block-grid[data-index-tab="2"]')
		const tabContentColTwo = document.querySelector('.Tabs__block-grid[data-index-tab="3"]')
		const tabContentRowColOne = document.querySelector('.Tabs__block-grid[data-index-tab="4"]')
		const tabContentAll = document.querySelectorAll('.Tabs__block-grid[data-index-tab]')

		const mediaDesc = window.matchMedia("(max-width: 1240px)")
		const mediaTablet = window.matchMedia("(max-width: 850px)")
		const mediaMobile = window.matchMedia("(max-width: 510px)")

		function resize(viewport, btnTab, tabContent, visibility="") {
			if (viewport.matches) {
				tabContentAll.forEach((item) => {
					item.classList.remove('Tabs__block-grid--active')
					item.classList.add('Tabs__block-grid--not-active')
					item.style.display = "none"
				})
	
				btnAll.forEach((item) => {
					item.classList.remove('Tabs__item-grid--active')
					item.classList.add('Tabs__item-grid--not-active')
					switch (visibility) {
						case "one":
							btnColThree.classList.add('V--hidden')
							break;
						case "two":
							btnColThree.classList.add('V--hidden')
							btnRowTwo.classList.add('V--hidden')
							break;
						case "three":
							item.classList.add('V--hidden')
							break;
					}

					
				})
				btnTab.classList.remove('Tabs__item-grid--not-active')
				btnTab.classList.remove('V--hidden')
				btnTab.classList.add('Tabs__item-grid--active')
				tabContent.classList.remove('Tabs__block-grid--not-active')
				tabContent.classList.add('Tabs__block-grid--active')
				tabContent.style.display = "grid"
			}
		}

		resize(mediaDesc,btnRowTwo,tabContentRowTwo, "one")
		resize(mediaTablet,btnColTwo,tabContentColTwo, "two")
		// resize(mediaMobile,btnRowColOne,tabContentRowColOne, "two")

	} catch {
    console.log("Ошибка initActiveSelectResize");
  }

}
