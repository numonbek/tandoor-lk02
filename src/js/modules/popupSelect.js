function popupSelect() {
  console.log("popupSelect");
  /* get data and render */
  try {
    const typeHandle = document.querySelector(".popup-selection-master--two");
    const selectType = document.querySelector(".popup-selection-master--four");

    const typeLoops = document.querySelector(".popup-selection-master--six");
    const typeCloser = document.querySelector(".popup-selection-master--eight");

    function container(container, block) {
      return container.querySelector(`.${block}`);
    }
    function checkInput(parentNode) {
      const parent = parentNode;
      // const radioBtn = document.querySelector(".Radio-btn-default");
      const radioInput = parent.querySelector(
        ".popup-selection-master__type-specification"
      );
      return radioInput.querySelectorAll("input");
    }

    const urlHandle = "http://localhost:9000/handle";
    const urlLoops = "http://localhost:9000/loops";
    const urlCloser = "http://localhost:9000/closer";
    // const testUrl = "https://json.medrating.org/users/";

    /* Defaults */
    let defImage = "../../img/default.jpg";
    let defText = "...";
    /* /Defaults */

    /* massive */

    let massiveHandle = [];
    let massiveLoops = [];
    let massiveCloser = [];

    /* /massive */

    function sendRequest(method, url) {
      try {
        const delay = (ms) => {
          return new Promise((r) => {
            return setTimeout(() => {
              r();
            }, ms);
          });
        };
        const headers = {
          "Content-type": "application/json",
        };
        const requestBody = {
          method: method,
          headers: headers,
          // body: JSON.parse(body),
        };
        return delay().then(() => {
          return fetch(url, requestBody).then((response) => {
            if (response.ok) {
              return response.json();
            }
            return response.json().then((error) => {
              const e = new Error("Что-то пошло не так");
              e.data = error;
              throw e;
            });
          });
        });
      } catch (err) {
        console.log("Error SendRequest");
      }
    }
    /* send request urlHandle */
    sendRequest("GET", urlHandle)
      .then((data) => {
        clearContent(typeHandle, "popup-selection-master__type-specification");
        data.forEach((dat, index) => {
          massiveHandle.push(dat);
          // massiveHandle.push({
          //   id: dat.id,
          //   picture: dat.picture || deafaultImage,
          // });
          // container(
          //   typeHandle,
          //   "popup-selection-master__type-specification"
          // ).innerHTML = "";
          container(
            typeHandle,
            "popup-selection-master__type-specification"
          ).insertAdjacentHTML(
            "beforeEnd",
            `<div class="Radio-btn-default">
                      <label class="Radio-btn-default__label">
                        <input class="Radio-btn-default__input" type="radio" name="type" value="" data-type-handle=${dat.type} data-id-handle=${dat.id}>
                        <span class="Radio-btn-default__inner-text">
                          <span class="Radio-btn-default__text">${dat.title}</span>
                        </span>
                      </label>
                    </div>`
          );
        });

        data.forEach((find, index) => {
          checkInput(typeHandle).forEach((item, index) => {
            item.addEventListener("click", (event) => {
              const targetHandle = event.target;

              if (targetHandle.dataset.idHandle == find.id) {
                let promise = new Promise((r) => {
                  container(
                    typeHandle,
                    "popup-selection-master__img"
                  ).setAttribute("src", `${find.picture || defImage}`);
                  r();
                });
                promise
                  .then(() => {
                    // container(
                    //   selectType,
                    //   "popup-selection-master__type-specification"
                    // ).innerHTML = "";
                    clearContent(
                      selectType,
                      "popup-selection-master__type-specification"
                    );
                    clearContent(
                      typeHandle,
                      "popup-selection-master__middle-content"
                    );
                    // container(
                    //   typeHandle,
                    //   "popup-selection-master__middle-content"
                    // ).innerHTML = "";
                    return container(
                      typeHandle,
                      "popup-selection-master__middle-content"
                    ).insertAdjacentHTML(
                      "beforeEnd",
                      `<div class="popup-selection-master__middle-heading">
									<div class="popup-selection-master__middle-title">
										<h3 class="popup-selection-master__middle-title-text">${
                      find.title || defText
                    }</h3>
									</div>
									<div class="popup-selection-master__middle-subtitle">
										<span class="popup-selection-master__middle-subtitle-text">
											${find.description || defText}
										</span>
									</div>
								</div>`
                    );
                  })
                  .then(() => {
                    container(
                      typeHandle,
                      "popup-swiper-btn-next-js"
                    ).classList.remove("popup__disabled");
                    // const btnDisabledPopup = querySelector(
                    //   ".popup-swiper-btn-next-js"
                    // );
                    // btnDisabledPopup

                    const masterRight = document.querySelectorAll(
                      ".popup-selection-master__list"
                    );
                    masterRight.forEach((type, index) => {
                      if (type.closest(".popup-selection-master--two")) {
                        type.innerHTML = "";
                        type.insertAdjacentHTML(
                          "beforeend",
                          `<li class="popup-selection-master__item popup-selection-master__item--active">
                                <div class="popup-selection-master__item-title">
                                  <h3 class="popup-selection-master__item-title-text">Тип ручки:</h3>
                                </div>
                                <div class="popup-selection-master__desc">
                                  <span class="popup-selection-master__desc-text">${find.title}</span>
                                </div>
                              </li>`
                        );
                      } else {
                        type.innerHTML = "";
                        type.insertAdjacentHTML(
                          "beforeend",
                          `<li class="popup-selection-master__item">
                            <div class="popup-selection-master__item-title">
                              <h3 class="popup-selection-master__item-title-text">Тип ручки: </h3>
                            </div>
                            <div class="popup-selection-master__desc">
                              <span class="popup-selection-master__desc-text">${find.title}</span>
                            </div>
                          </li>`
                        );
                      }
                    });
                  })
                  .then(() => {
                    find.children.forEach((child, inChild) => {
                      container(
                        selectType,
                        "popup-selection-master__type-specification"
                      ).insertAdjacentHTML(
                        "beforeEnd",
                        `<div class="Radio-btn-default">
                                  <label class="Radio-btn-default__label">
                                    <input class="Radio-btn-default__input" type="radio" name="type" value="" data-type-select=${
                                      child.type || null
                                    } data-id-select=${child.id}>
                                    <span class="Radio-btn-default__inner-text">
                                      <span class="Radio-btn-default__text">${
                                        child.title
                                      }</span>
                                    </span>
                                  </label>
                                </div>`
                      );
                    });
                    find.children.forEach((childs, inChild) => {
                      checkInput(selectType).forEach((item) => {
                        item.addEventListener("click", (event) => {
                          const targetHandlee = event.target;
                          console.log("children", targetHandlee);
                          if (targetHandlee.dataset.idSelect == childs.id) {
                            container(
                              selectType,
                              "popup-selection-master__img"
                            ).setAttribute(
                              "src",
                              `${childs.picture || defImage}`
                            );
                            container(
                              selectType,
                              "popup-selection-master__middle-content"
                            ).innerHTML = middleContent(childs);
                            const masterRight = document.querySelectorAll(
                              ".popup-selection-master__list"
                            );
                            const masterMain = document.querySelector(
                              ".popup-selection-master__list"
                            );
                            container(
                              selectType,
                              "popup-swiper-btn-next-js"
                            ).classList.remove("popup__disabled");

                            masterRight.forEach((type, index) => {
                              const liCountOne =
                                masterRight[0].childNodes.length - 1;
                              for (let i = liCountOne; i > 0; i--) {
                                masterRight[0].childNodes[i].remove();
                              }
                              const liCountTwo =
                                masterRight[1].childNodes.length - 2;
                              for (let i = liCountTwo; i > 0; --i) {
                                masterRight[1].childNodes[i].remove();
                              }

                              const liCountThree =
                                masterRight[2].childNodes.length - 2;
                              for (let i = liCountThree; i > 0; --i) {
                                masterRight[2].childNodes[i].remove();
                              }
                              const liCountFour =
                                masterRight[3].childNodes.length - 1;
                              for (let i = liCountFour; i > 0; --i) {
                                masterRight[3].childNodes[i].remove();
                              }

                              if (
                                type.closest(".popup-selection-master--four")
                              ) {
                                type.insertAdjacentHTML(
                                  "beforeend",

                                  `<li class="popup-selection-master__item popup-selection-master__item--active">
                                  <div class="popup-selection-master__item-title">
                                    <h3 class="popup-selection-master__item-title-text">Ручка: </h3>
                                  </div>
                                  <div class="popup-selection-master__desc">
                                    <span class="popup-selection-master__desc-text">
                                     ${childs.title}
                                      <span class="popup-selection-master__desc-text-decor">
                                        <span data-popup-first="">${childs.price}</span> ₽
                                      </span>
                                      <span data-first-child="">1</span>шт.
                                    </span>
                                  </div>
                                </li>`
                                );
                              } else {
                                type.insertAdjacentHTML(
                                  "beforeend",
                                  `<li class="popup-selection-master__item">
                                  <div class="popup-selection-master__item-title">
                                    <h3 class="popup-selection-master__item-title-text">Ручка: </h3>
                                  </div>
                                  <div class="popup-selection-master__desc">
                                    <span class="popup-selection-master__desc-text">
                                    ${childs.title}
                                      <span class="popup-selection-master__desc-text-decor">
                                        <span data-popup-first="">${childs.price}</span> ₽
                                      </span>
                                      <span data-first-child="">1</span>шт.
                                    </span>
                                  </div>
                                </li>`
                                );
                              }
                            });
                          }
                        });
                      });
                    });
                  });
              }
            });
          });
        });
      })
      .catch((e) => console.log(e));

    /* send request urlLoops */
    sendRequest("GET", urlLoops)
      .then((data) => {
        clearContent(typeLoops, "popup-selection-master__type-specification");
        data.forEach((loop) => {
          container(
            typeLoops,
            "popup-selection-master__type-specification"
          ).insertAdjacentHTML(
            "beforeEnd",
            `<div class="Radio-btn-default">
                      <label class="Radio-btn-default__label">
                        <input class="Radio-btn-default__input" type="radio" name="type" value="" data-type-loops=${loop.type} data-id-loops=${loop.id}>
                        <span class="Radio-btn-default__inner-text">
                          <span class="Radio-btn-default__text">${loop.title}</span>
                        </span>
                      </label>
                    </div>`
          );
        });

        data.forEach((loop) => {
          checkInput(typeLoops).forEach((item) => {
            item.addEventListener("click", (event) => {
              const targetLoops = event.target;
              if (targetLoops.dataset.idLoops == loop.id) {
                container(
                  typeLoops,
                  "popup-selection-master__img"
                ).setAttribute("src", `${loop.picture || defImage}`);
                container(
                  typeLoops,
                  "popup-selection-master__middle-content"
                ).innerHTML = middleContent(loop);

                const masterRight = document.querySelectorAll(
                  ".popup-selection-master__list"
                );
                container(
                  typeLoops,
                  "popup-swiper-btn-next-js"
                ).classList.remove("popup__disabled");
                masterRight.forEach((type, index) => {
                  // const checkType =
                  //   type.lastChild.querySelector(
                  //     ".popup-selection-master__item-title-text"
                  //   ).innerText == "Ручка:";
                  // if (!checkType) {
                  //   type.lastChild.remove();
                  // }
                  const liCountLoopsOne = masterRight[0].childNodes.length - 1;
                  for (let i = liCountLoopsOne; i > 0; i--) {
                    masterRight[0].childNodes[i].remove();
                  }
                  const liCountLoopsTwo = masterRight[1].childNodes.length - 1;
                  for (let i = liCountLoopsTwo; i > 1; --i) {
                    masterRight[1].childNodes[i].remove();
                  }

                  const liCountLoopsThree =
                    masterRight[2].childNodes.length - 2;
                  for (let i = liCountLoopsThree; i > 1; --i) {
                    masterRight[2].childNodes[i].remove();
                  }
                  const liCountLoopsFour = masterRight[3].childNodes.length - 1;
                  for (let i = liCountLoopsFour; i > 1; --i) {
                    masterRight[3].childNodes[i].remove();
                  }

                  if (type.closest(".popup-selection-master--six")) {
                    type.insertAdjacentHTML(
                      "beforeend",

                      `<li class="popup-selection-master__item popup-selection-master__item--active">
                      <div class="popup-selection-master__item-title">
                        <h3 class="popup-selection-master__item-title-text">Петли: </h3>
                      </div>
                      <div class="popup-selection-master__desc">
                        <span class="popup-selection-master__desc-text">
                          ${loop.title}
                          <span class="popup-selection-master__desc-text-decor">
                            <span data-popup-two="">${loop.price}</span> ₽
                          </span>
                          <span data-two-child="">1</span>шт.
                        </span>
                      </div>
                    </li>`
                    );
                  } else {
                    type.insertAdjacentHTML(
                      "beforeend",
                      `<li class="popup-selection-master__item">
                      <div class="popup-selection-master__item-title">
                        <h3 class="popup-selection-master__item-title-text">Петли: </h3>
                      </div>
                      <div class="popup-selection-master__desc">
                        <span class="popup-selection-master__desc-text">
                          ${loop.title}
                          <span class="popup-selection-master__desc-text-decor">
                            <span data-popup-two="">${loop.price}</span> ₽
                          </span>
                          <span data-two-child="">1</span>шт.
                        </span>
                      </div>
                    </li>`
                    );
                  }
                });
              }
            });
          });
        });
      })
      .catch((e) => console.log(e));

    /* send request urlCloser */
    sendRequest("GET", urlCloser)
      .then((data) => {
        clearContent(typeCloser, "popup-selection-master__type-specification");
        data.forEach((close) => {
          container(
            typeCloser,
            "popup-selection-master__type-specification"
          ).insertAdjacentHTML(
            "beforeEnd",
            `<div class="Radio-btn-default">
                      <label class="Radio-btn-default__label">
                        <input class="Radio-btn-default__input" type="radio" name="type" value="" data-type-closer=${close.type} data-id-closer=${close.id}>
                        <span class="Radio-btn-default__inner-text">
                          <span class="Radio-btn-default__text">${close.title}</span>
                        </span>
                      </label>
                    </div>`
          );
        });

        data.forEach((close) => {
          checkInput(typeCloser).forEach((item) => {
            item.addEventListener("click", (event) => {
              const targetLoops = event.target;
              if (targetLoops.dataset.idCloser == close.id) {
                container(
                  typeCloser,
                  "popup-selection-master__img"
                ).setAttribute("src", `${close.picture || defImage}`);
                container(
                  typeCloser,
                  "popup-selection-master__middle-content"
                ).innerHTML = middleContent(close);

                const masterRight = document.querySelectorAll(
                  ".popup-selection-master__list"
                );

                container(typeCloser, "Button--lime").classList.remove(
                  "popup__disabled"
                );

                masterRight.forEach((type, index) => {
                  // const checkType =
                  //   type.lastChild.querySelector(
                  //     ".popup-selection-master__item-title-text"
                  //   ).innerText == "Петли:";
                  // if (!checkType) {
                  //   type.lastChild.remove();
                  // }

                  const liCountCloserOne = masterRight[0].childNodes.length - 1;
                  for (let i = liCountCloserOne; i > 0; i--) {
                    masterRight[0].childNodes[i].remove();
                  }
                  const liCountCloserTwo = masterRight[1].childNodes.length - 1;
                  for (let i = liCountCloserTwo; i > 1; --i) {
                    masterRight[1].childNodes[i].remove();
                  }

                  const liCountCloserThree =
                    masterRight[2].childNodes.length - 1;
                  for (let i = liCountCloserThree; i > 2; --i) {
                    masterRight[2].childNodes[i].remove();
                  }
                  const liCountCloserFour =
                    masterRight[3].childNodes.length - 1;
                  for (let i = liCountCloserFour; i > 2; --i) {
                    masterRight[3].childNodes[i].remove();
                  }

                  if (type.closest(".popup-selection-master--eight")) {
                    type.insertAdjacentHTML(
                      "beforeend",

                      `<li class="popup-selection-master__item popup-selection-master__item--active">
                      <div class="popup-selection-master__item-title">
                        <h3 class="popup-selection-master__item-title-text">Доводчик:</h3>
                      </div>
                      <div class="popup-selection-master__desc">
                        <span class="popup-selection-master__desc-text">
                          ${close.title}
                          <span class="popup-selection-master__desc-text-decor">
                            <span data-popup-third="">${close.price}</span> ₽
                          </span>
                          <span data-third-child="">1</span>шт.
                        </span>
                      </div>
                    </li>`
                    );
                  } else {
                    type.insertAdjacentHTML(
                      "beforeend",
                      `<li class="popup-selection-master__item">
                      <div class="popup-selection-master__item-title">
                        <h3 class="popup-selection-master__item-title-text">Доводчик: </h3>
                      </div>
                      <div class="popup-selection-master__desc">
                        <span class="popup-selection-master__desc-text">
                          ${close.title}
                          <span class="popup-selection-master__desc-text-decor">
                            <span data-popup-third="">${close.price}</span> ₽
                          </span>
                          <span data-third-child="">1</span>шт.
                        </span>
                      </div>
                    </li>`
                    );
                  }
                  // popupTotal();
                });
              }
            });
          });
        });
      })
      .catch((e) => console.log(e));

    /* Clear Function */
    function clearContent(data, classs) {
      let clear = container(data, `${classs}`);
      return (clear.innerHTML = "");
    }

    /* Middle Content Counter */
    function middleContent(data) {
      return `<div class="popup-selection-master__middle-heading">
      <div class="popup-selection-master__middle-title">
        <h3 class="popup-selection-master__middle-title-text">${
          data.title || defText
        }</h3>
      </div>
    </div>
    <div class="popup-selection-master__Counter Counter">
      <div class="popup-selection-master__counter-title Counter__value">
        <span class="popup-selection-master__counter-title-text Counter__value-text">
          <span data-popup-counter="">${data.price || 0}</span> ₽</span>
      </div>
      <div class="Counter__container">
        <div class="Counter__content">
          <button class="Counter__button Counter__button--js-reduction" data-decrement="">
            <svg class="Counter__svg">
              <use xlink:href="#arrow-left" class="Counter__use"></use>
            </svg>
          </button>
          <div class="Counter__block-input">
            <input class="Counter__input Counter__input--js" type="text" value="1" min="1" max="${
              data.stock || 0
            }" step="1">
            <span class="Counter__dimension">${data.stock || 0} в наличии</span>
          </div>
          <button class="Counter__button Counter__button--js-increase" data-increment="">
            <svg class="Counter__svg">
              <use xlink:href="#arrow-right" class="Counter__use"></use>
            </svg>
          </button>
        </div>
      </div>
     </div>
    `;
    }
  } catch (err) {
    console.log("Error popupSelect");
  }
}
