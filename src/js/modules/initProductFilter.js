function initProductFilter() {
  console.log('initProductFilter');

  const btnFilter = document.querySelector('[data-filter]');
  const filter = document.querySelector('.product-filter');
  const blackout = document.querySelector('.blackout');

  if (!filter) return;

  btnFilter.addEventListener('click', () => {
    filter.classList.add('product-filter--active');
    blackout.classList.add('blackout--active');
  });

  document.addEventListener('click', (event) => {
    if (
      (!event.target.closest('.product-filter--active') &&
        !event.target.closest('[data-filter]')) ||
      event.target.closest('[data-filter-icon-close]')
    ) {
      filter.classList.remove('product-filter--active');
      blackout.classList.remove('blackout--active');
    }
  });

  const listItem = document.querySelectorAll('.product-filter__li');

  listItem.forEach((item) => {
    item.addEventListener('click', (event) => {
      event.stopPropagation();
      if (item.parentNode.hasAttribute('data-list-lvl-one')) {
        if (!item.querySelector('.product-filter__list[data-list-lvl-two]'))
          return;
        if (
          !item
            .querySelector('.product-filter__list[data-list-lvl-two]')
            .classList.contains('no-js')
        ) {
          const list = item.querySelector(
            '.product-filter__list[data-list-lvl-two]'
          );
          $(list).slideToggle(300);
          item
            .querySelector('.product-filter__li-icon-arrow')
            .classList.toggle('product-filter__li-icon-arrow--active');
        }
      }
    });
  });
}
