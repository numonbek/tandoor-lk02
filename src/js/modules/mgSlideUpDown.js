function mgSlideUpDown() {
    console.log('slideUpDown');

    try {
        /* forDropDown for any */
        const forDropDowns = function() {
            $('.parent-menu').on('click', function(e) {
                $(this).next('.child-menu').slideToggle(300);
                $(this).toggleClass('toggle-rotate');
            });
        };
        forDropDowns();

        /* for One forDropDown */
        const forDropDown = function() {
            $('.parent-down').on('click', function(e) {
                // console.log('this', $(this).siblings());
                $(this).next('.child-down').slideToggle(300);
                $(this).toggleClass('toggle-rotate');
            });
        };
        forDropDown();
    } catch (err) {
        console.log(' ERROR slideUpDown');
    }
}