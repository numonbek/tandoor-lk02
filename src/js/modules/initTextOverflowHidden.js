function initTextOverflowHidden(e) {
	try {
		const node = document.querySelectorAll(".text-overflow-ellipsis-js");
		const nodeBody = document.querySelector(".text-overflow-js");
		let needСharacters = Number(nodeBody.getAttribute("data-text-sum"));
		console.log(needСharacters);
		[].forEach.call(node, function (data) {
			if (data.innerHTML.length > needСharacters) {
				data.innerHTML = data.innerHTML.slice(0, needСharacters) + "…";
			}
		});
	}

	catch {
		console.log("Ошибка initTextOverflowHidden()");
	}
}