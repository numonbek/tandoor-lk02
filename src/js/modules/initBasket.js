function initBasket() {
  console.log("initBasket");
  try {
    document.addEventListener("click", (e) => {
      const closeBtn = e.target.closest(".item-Card-buy__remove-product-js");

      if (!closeBtn) return;

      const productCard = closeBtn.closest(".Card-buy__wrapper-item-products");

      if (!productCard) return;

      if (closeBtn) {
        productCard.classList.add("hidden");
      }
    });


    document.addEventListener("click", (e) => {
      const closeBtnProduct = e.target.closest(".item-Card-buy__remove-item-js");

      if (!closeBtnProduct) return;

      const productCardItem = closeBtnProduct.closest(".item-Card-buy__item-detailed");

      if (!productCardItem) return;

      if (closeBtnProduct) {
        productCardItem.classList.add("hidden");
      }
    });

  } catch {
    console.log("Ошибка initBasket");
  }
}
