function initPopupsClearDN() {
  console.log("initPopupsClearDN");

	const blockElements = document.querySelectorAll('.gui-page__block-element')
	if(!blockElements) return console.log('Error initPopupsClearDN')
	let popup = ''
	blockElements.forEach((item) => {
		popup = item.querySelector('[style]')
		if (popup) {
			popup.removeAttribute('style')
		}
	})
}
