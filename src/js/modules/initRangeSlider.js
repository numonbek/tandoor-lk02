function initRangeSlider() {
  console.log('initRangeSlider');

  var stepsSlider = document.getElementById('steps-slider');
  if (!stepsSlider) return;
  var input0 = document.getElementById('input-with-keypress-0');
  var input1 = document.getElementById('input-with-keypress-1');
  var inputs = [input0, input1];

  var min = document.querySelector('.range-slider__value--min-js');
  var max = document.querySelector('.range-slider__value--max-js');

  min.innerHTML =
    parseFloat(stepsSlider.getAttribute('data-range-min')).toLocaleString(
      'ru-RU'
    ) + ' ₽';
  max.innerHTML =
    parseFloat(stepsSlider.getAttribute('data-range-max')).toLocaleString(
      'ru-RU'
    ) + ' ₽';

  noUiSlider.create(stepsSlider, {
    start: [
      +input0.getAttribute('data-range-start'),
      +input1.getAttribute('data-range-end'),
    ],
    connect: true,
    range: {
      min: [+stepsSlider.getAttribute('data-range-min')],
      '100%': [1, 1],
      max: +stepsSlider.getAttribute('data-range-max'),
    },
  });

  stepsSlider.noUiSlider.on('update', function (values, handle) {
    // inputs[handle].value = parseFloat(
    //   Math.round(values[handle])
    // ).toLocaleString('ru-RU');
    inputs[handle].value = Math.round(values[handle]);
  });

  // Listen to keydown events on the input field.
  inputs.forEach(function (input, handle) {
    input.addEventListener('change', function () {
      stepsSlider.noUiSlider.setHandle(handle, this.value);
    });

    input.addEventListener('keydown', function (e) {
      var values = stepsSlider.noUiSlider.get();
      var value = Number(values[handle]);

      // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
      var steps = stepsSlider.noUiSlider.steps();

      // [down, up]
      var step = steps[handle];

      var position;

      // 13 is enter,
      // 38 is key up,
      // 40 is key down.
      switch (e.which) {
        case 13:
          e.preventDefault();
          stepsSlider.noUiSlider.setHandle(handle, this.value);
          break;

        case 38:
          // Get step to go increase slider value (up)
          position = step[1];

          // false = no step is set
          if (position === false) {
            position = 1;
          }

          // null = edge of slider
          if (position !== null) {
            stepsSlider.noUiSlider.setHandle(handle, value + position);
          }

          break;

        case 40:
          position = step[0];

          if (position === false) {
            position = 1;
          }

          if (position !== null) {
            stepsSlider.noUiSlider.setHandle(handle, value - position);
          }

          break;
      }
    });
  });
}
