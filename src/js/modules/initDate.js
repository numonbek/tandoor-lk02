function initDate() {
  console.log("initDate");
  try {
		$('#calendar').datepicker({
			multipleDates: 2, //! Выбор нескольких дат
			multipleDatesSeparator: ' - ', //! Разделитель при выборе нескольких дат
			minDate: new Date(), //! Выбирать можно только с текущей даты
			clearButton: true,  //! Кнопка очистить
			position: 'bottom right', //! Позиция календаря
			view: 'days', //! Начальное отображение календаря (месяц, год, дни)
			firstDay: 1, //! С какого дня начинается неделя (где 0, это воскресенье)
			selectOtherMonths: true, //! Если true, то можно будет выбрать дни из других месяцев.
			todayButton: new Date(), //! Кнопка "Сегодня"
			showEvent: "focus", //!Тип события, по наступлению которого будет показан календарь.

			//! Методы

			onSelect(formatteDate,date,inst) {
				inst.hide();
			}

		});

		$('#calendar-one').datepicker({
			// multipleDates: 2, //! Выбор нескольких дат
			// multipleDatesSeparator: ' - ', //! Разделитель при выборе нескольких дат
			minDate: new Date(), //! Выбирать можно только с текущей даты
			clearButton: true,  //! Кнопка очистить
			position: 'bottom center', //! Позиция календаря
			view: 'days', //! Начальное отображение календаря (месяц, год, дни)
			firstDay: 1, //! С какого дня начинается неделя (где 0, это воскресенье)
			selectOtherMonths: true, //! Если true, то можно будет выбрать дни из других месяцев.
			todayButton: new Date(), //! Кнопка "Сегодня"
			showEvent: "focus", //!Тип события, по наступлению которого будет показан календарь.

			//! Методы

			onSelect(formatteDate,date,inst) {
				inst.hide();
			}

		});
		$('#calendar-two').datepicker({
			// multipleDates: 2, //! Выбор нескольких дат
			// multipleDatesSeparator: ' - ', //! Разделитель при выборе нескольких дат
			minDate: new Date(), //! Выбирать можно только с текущей даты
			clearButton: true,  //! Кнопка очистить
			position: 'top center', //! Позиция календаря
			view: 'days', //! Начальное отображение календаря (месяц, год, дни)
			firstDay: 1, //! С какого дня начинается неделя (где 0, это воскресенье)
			selectOtherMonths: true, //! Если true, то можно будет выбрать дни из других месяцев.
			todayButton: new Date(), //! Кнопка "Сегодня"
			showEvent: "focus", //!Тип события, по наступлению которого будет показан календарь.

			//! Методы

			onSelect(formatteDate,date,inst) {
				inst.hide();
			}

		});

  } catch {
    console.log("Ошибка initDate");
  }
}
