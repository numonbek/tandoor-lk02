function mgPortfolio() {
  const data = [];

  let editBlock = document.querySelector('.popup-img__edit-block');
  let cropper;
  let portfolioItem = document.querySelectorAll('.mg-portfolio-box__item');
  const buttonAdd = document.querySelectorAll('.add-attendance button');
  let modalImage = document.querySelector('#img-area-modal-slider');
  const modal = document.querySelector('.mg-modal-slide');
  const windowCrop = document.querySelector('.popup-img__edit-block__edit');
  const windowImg = document.querySelector('.popup-img__edit-block__filter');
  const windowRotate = document.querySelector('.popup-img__edit-block__crop');
  /* txt */
  const description = document.querySelector('.popup-img__textarea');
  const mgCropNext = modal.querySelector('.swiper-button-edit-text-next');
  const mgCrop = modal.querySelector('.mg-crop');

  /* modalpopup */
  const modalPopupSlide = document.querySelector('.mg-modal-edit');
  const descImg = modalPopupSlide.querySelector('.Img__item');
  const desc = modalPopupSlide.querySelector('.desc');
  const descTextArea = modalPopupSlide.querySelector('.Text-area');
  const descBtn = modalPopupSlide.querySelectorAll('[data-method]');

  let descImgId;

  let targetBlock;
  let indexAlbum = 0;

  let fakeInput = document.createElement('input');
  fakeInput.type = 'file';
  fakeInput.accept = 'image/*';
  fakeInput.multiple = true;

  buttonAdd.forEach((btn, index) => {
    btn.addEventListener('click', (e) => {
      fakeInput.value = null;
      fakeInput.click();
      // indexAlbum = index;
      // targetBlock = e.target.closest('[data-gallery-id]');
    });
  });

  fakeInput.addEventListener('input', (e) => {
    e.preventDefault();
    let img = e.target.files[0];

    if (e.target.files.length) {
      modal.classList.add('show');
      description.value = '';
      var swiperPopup = new Swiper('.mySwiper', {
        spaceBetween: 10,
        centeredSlides: true,

        allowTouchMove: false,
        navigation: {
          nextEl: '.swiper-button-edit-text-next',
          prevEl: '.swiper-button-edit-photo-prev',
        },
      });
      // start file reader
      const reader = new FileReader();
      reader.onload = (e) => {
        if (e.target.result) {
          modalImage.src = e.target.result;
          cropper = new Cropper(modalImage, {
            aspectRatio: 1,
            viewMode: 1,
            resizable: true,
            zoomable: true,
          });
          modalImage.classList.add('cropper-container');
        }
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  });

  editBlock.addEventListener('click', function (e) {
    const target = e.target;
    const methods = target.dataset.method;
    if (modalImage.classList.contains('cropper-container')) {
      switch (methods) {
        case 'crop':
          let imgSrc = cropper
            .getCroppedCanvas({
              width: 400,
              height: 400,
            })
            .toDataURL();
          modalImage.src = imgSrc;
          cropper.destroy();
          modalImage.classList.remove('cropper-container');
          windowRotate.setAttribute('data-option', 0);
          windowRotate.style.transform = `rotate(${+target.dataset.option}deg)`;
          windowImg.setAttribute('data-option', 1);
          windowImg.style.transform = `scaleX(1)`;
          break;
        case 'rotate':
          cropper.rotate(45);
          target.setAttribute('data-option', +target.dataset.option + 45);
          target.style.transform = `rotate(${+target.dataset.option}deg)`;
          break;
        case 'scaleX':
          target.setAttribute('data-option', -parseInt(target.dataset.option));
          cropper.scaleX(+target.dataset.option);
          target.style.transform = `scaleX(${+target.dataset.option})`;
          break;
        default:
          break;
      }
    } else {
      cropper = new Cropper(modalImage, {
        aspectRatio: 1,
        viewMode: 1,
        resizable: true,
        zoomable: true,
      });
      modalImage.classList.add('cropper-container');
    }
  });
  /* window Click area */
  window.addEventListener('click', (e) => {
    if (e.target.closest('[data-gallery-id]')) {
      targetBlock = e.target.closest('[data-gallery-id]');
      indexAlbum = data.findIndex((el) => el.id == targetBlock.dataset.galleryId);
    }

    if (e.target.closest('.att-eye')) {
      const imgId = e.target.closest('[data-image-id]');
      modalPopupSlide.style.display = 'flex';
      descImgId = imgId.dataset.imageId;
      data[indexAlbum].dataImg.map((el, i) => {
        if (el.imageId == descImgId) {
          descImg.src = el.img;
          descTextArea.value = el.text;
        }
      });
    }

    if (e.target.closest('.att-edit')) {
      modalPopupSlide.style.display = 'flex';
      desc.classList.add('desc--submit');

      const imgId = e.target.closest('[data-image-id]');

      descImgId = imgId.dataset.imageId;

      data[indexAlbum].dataImg.map((el, i) => {
        if (el.imageId == descImgId) {
          descImg.src = el.img;
          descTextArea.value = el.text;
        }
      });
    }

    if (e.target.closest('.att-trash')) {
      const targetBlock = e.target.closest('[data-gallery-id]');
      const indexAlbum = data.findIndex((el) => el.id == targetBlock.dataset.galleryId);
      const imgBlock = e.target.closest('.attendance-photo__list');
      const imgId = imgBlock.dataset.imageId;

      data[indexAlbum].dataImg = data[indexAlbum].dataImg.filter((el) => el.imageId != imgId);
      imgBlock.remove();
    }

    /* for Modal Popup */
    if (e.target.closest('.mg-modal-edit')) {
      if (!e.target.closest('.popup-slider') || e.target.closest('.bt-popup-slider-cancel')) {
        modalPopupSlide.style.display = 'none';
        desc.classList.remove('desc--submit');
      } else {
        switch (e.target.closest('[data-method]').dataset.method) {
          case 'cancel':
            modalPopupSlide.style.display = 'none';
            desc.classList.remove('desc--submit');
            break;
          case 'save':
            desc.classList.remove('desc--submit');
            data[indexAlbum].dataImg = data[indexAlbum].dataImg.map((el, i) =>
              el.imageId == descImgId ? { ...el, img: descImg.src, text: descTextArea.value } : el,
            );
            break;
          case 'edit':
            desc.classList.add('desc--submit');
            break;
          case 'delete':
            data[indexAlbum].dataImg = data[indexAlbum].dataImg.filter(
              (el) => el.imageId != descImgId,
            );
            renderAlbum(data, targetBlock, indexAlbum);
            modalPopupSlide.style.display = 'none';
            desc.classList.remove('desc--submit');
            break;
          default:
            break;
        }
      }
    }

    /* for Modal slide */
    if (!e.target.closest('.mg-modal-slide')) return false;
    if (!e.target.closest('.popup-img') || e.target.closest('.mg-cancel')) {
      modal.classList.remove('show');
      modal.style.display = 'none';
      cropper.destroy();
      modalImage.classList.remove('cropper-container');
    }
    if (e.target.closest('.swiper-button-edit-text-next')) {
      let imgSrc = cropper
        .getCroppedCanvas({
          width: 400,
          height: 400,
        })
        .toDataURL();
      modalImage.src = imgSrc;
      cropper.destroy();
    }
    /*  */
    if (modal.classList.contains('show')) {
      if (e.target.closest('.mg-crop')) {
        const state = {
          id: targetBlock.dataset.galleryId,
          name: targetBlock.dataset.galleryName,
          dataImg: [
            {
              imageId: Math.floor(Math.random() * 100000),
              img: modalImage.src,
              text: description.value,
            },
          ],
        };
        if (indexAlbum < 0) {
          data.push(state);
          indexAlbum = data.length - 1;
        } else {
          for (let i = 0; i < data.length; i++) {
            if (data[i].id == targetBlock.dataset.galleryId) {
              data[i].dataImg.push({
                imageId: Math.floor(Math.random() * 100000),
                img: modalImage.src,
                text: description.value,
              });
            }
          }
        }

        modal.classList.remove('show');
        modal.style.display = 'none';
        modalImage.classList.remove('cropper-container');

        renderAlbum(data, targetBlock, indexAlbum);
      }
    }
  });
}

const renderAlbum = (data, targetBlock, indexAlbum) => {
  const content = targetBlock.querySelector('.attendance-photo__container');
  content.innerHTML = '';
  data[indexAlbum].dataImg.map((el, i) => renderImages(content, el));
};

const renderImages = (content, el) => {
  content.insertAdjacentHTML(
    'beforeend',
    `<li class="attendance-photo__list" data-image-id=${el.imageId}>
  <img src=${el.img}
      alt="">
  <span style="display: none;">${el.text}</span>
  <span class="att-edit-block">
      <span class="att-eye">
          <svg class="Button__icon-svg">
              <use class="Button__icon-svg-use" xlink:href="#eye-open">
              </use>
          </svg>
      </span>
      <span class="att-edit">
          <svg class="Button__icon-svg">
              <use class="Button__icon-svg-use" xlink:href="#edit">
              </use>
          </svg>
      </span>
      <span class="att-trash">
          <svg class="Button__icon-svg">
              <use class="Button__icon-svg-use" xlink:href="#trash">
              </use>
          </svg>
      </span>
  </span>
  </li>`,
  );
};
