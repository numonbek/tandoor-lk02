function initLogininOpen() {
  console.log("initLogininOpen");
  const headerIconPhone = document.querySelectorAll(".header__block-loginin");
  const headerDopPhones = document.querySelectorAll(".header__loginin-popup");
  const ButtonCatalog = document.querySelectorAll(".header__button-catalog");
  const HeaderCatalog = document.querySelectorAll(".header-catalog");
  // data-loginin

  let phoneIconIndex;
  let ButtonCatalogIndex;

  headerIconPhone.forEach(function (itemIcon) {
    itemIcon.addEventListener("click", function () {
      itemIcon.classList.toggle("header__block-loginin--open");
    });
    headerDopPhones.forEach(function (itemDopPhone) {
      itemIcon.addEventListener("click", function () {
        phoneIconIndex = itemIcon.getAttribute("data-loginin");

        if (phoneIconIndex === itemDopPhone.getAttribute("data-loginin")) {
          itemDopPhone.classList.toggle("header__loginin-popup--open");
        }
      });

      document.addEventListener("mousedown", function (event) {
        let phoneStatus = "";
        let blockPhoneStatus = "";

        if (itemDopPhone.getAttribute("data-loginin") === phoneIconIndex) {
          blockPhoneStatus = itemDopPhone.contains(event.target);
        }

        phoneStatus = itemIcon.contains(event.target);

        if (
          phoneStatus === false &&
          blockPhoneStatus === false &&
          phoneIconIndex === itemIcon.getAttribute("data-loginin")
        ) {
          itemIcon.classList.remove("header__block-loginin--open");
          itemDopPhone.classList.remove("header__loginin-popup--open");
        }
      });
    });
  });


  ButtonCatalog.forEach(function (itemIcon) {
    itemIcon.addEventListener("click", function () {
      itemIcon.classList.toggle("header-catalog--open");
    });
    HeaderCatalog.forEach(function (itemHeaderCatalog) {
      itemIcon.addEventListener("click", function () {
        ButtonCatalogIndex = itemIcon.getAttribute("data-header-catalog");

        if (ButtonCatalogIndex === HeaderCatalog.getAttribute("data-header-catalog")) {
          HeaderCatalog.classList.toggle("header-catalog--open");
        }
      });

      document.addEventListener("mousedown", function (event) {
        let phoneStatus = "";
        let blockPhoneStatus = "";

        if (HeaderCatalog.getAttribute("data-header-catalog") === ButtonCatalogIndex) {
          blockPhoneStatus = HeaderCatalog.contains(event.target);
        }

        phoneStatus = itemIcon.contains(event.target);

        if (
          phoneStatus === false &&
          blockPhoneStatus === false &&
          ButtonCatalogIndex === itemIcon.getAttribute("data-header-catalog")
        ) {
          itemIcon.classList.remove("header-catalog--open");
          HeaderCatalog.classList.remove("header-catalog--open");
        }
      });
    });
  });
}
