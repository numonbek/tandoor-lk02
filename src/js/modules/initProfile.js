function initProfile() {
  console.log("initProfile");
  try {
    const btnEdit = document.querySelector(".profile-section__btn-edit-js");
    const form = document.querySelector(".profile-section__form--js");
    const nameElemValue = document.querySelectorAll(
      ".profile-section__name-element-value--js"
    );
    const inputs = document.querySelectorAll(".profile-section__input--js");

    btnEdit.addEventListener("click", () => {
      form.classList.add("profile-section__form--active");

      inputs.forEach((itemInput, indexInput) => {
        itemInput.value = nameElemValue[indexInput].textContent;
      });
    });
  } catch (e) {
    console.log("Ошибка initProfile" + e);
  }
}
