function validationSocial() {
  const selector = document.querySelectorAll('.mg-socials');

  const instagram = /^(https?:\/\/){0,1}(www\.){0,1}instagram\.com\/[a-z\d-_]{1,255}\s*$/i;
  const facebook =
    /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:facebook\.com|fb.com))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;
  const vk = /^(https{0,1}:\/\/)?(www\.)?(vk.com\/)(id\d|[a-zA-z][a-zA-Z0-9_.]{2,})?$/;
  const youtube =
    /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;
  const telegram = /.*?\B@\w{5}.*/;
  const tiktok =
    /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:tiktok\.com|tiktok.com))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;

  selector.forEach((item, index) => {
    item.addEventListener('input', (e) => {
      switch (e.target.name.toLowerCase()) {
        case 'instagram':
          checkSocial(instagram, e.target);
          break;
        case 'facebook':
          checkSocial(facebook, e.target);
          break;
        case 'vk':
          checkSocial(vk, e.target);
          break;
        case 'youtube':
          checkSocial(youtube, e.target);
          break;
        case 'telegram':
          checkSocial(telegram, e.target);
          break;
        case 'tiktok':
          checkSocial(tiktok, e.target);
          break;
        default:
          break;
      }
    });
  });

  function checkSocial(social, event) {
    if (social.exec(event.value)) {
      event.style.borderColor = '#9aca3c';
      event.classList.add('matched');
      console.log('match');
    } else {
      let checkName = event.name.toLowerCase();
      if (checkName == 'youtube' || checkName == 'tiktok' || checkName == 'telegram') {
        // event.style.borderColor = '#F9A602';
      } else {
        event.style.borderColor = '#d84040';
        event.classList.remove('matched');
        console.log('not match');
      }
    }
  }
}
