function initInputEye() {
  console.log("initInputEye");
  try {
    function inputEye(mainElem, elem, oldName, newName, input) {
      document.addEventListener("mousedown", (event) => {
        if (event.target.closest(mainElem) && event.target.closest(elem)) {
          const mainPath = event.target.closest(mainElem);
          const mainEye = mainPath.querySelector(elem);
          const mainInput = mainPath.querySelector(input);

          if (mainInput.getAttribute("type") != "password") {
            mainInput.setAttribute("type", "password");
            mainEye.querySelector("use").setAttribute("xlink:href", oldName);
          } else {
            mainInput.setAttribute("type", "text");
            mainEye.querySelector("use").setAttribute("xlink:href", newName);
          }
        }
      });
    }

    inputEye(
      ".Input__label--js",
      ".Input__icon--js",
      "#eye-hidden",
      "#eye-open",
      ".Input__inp--js"
    );
  } catch (e) {
    console.log("Ошибка initInputEye", e);
  }
}
