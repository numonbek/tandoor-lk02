function initInputmask() {
  console.log("initInputmask");
  let phone = document.querySelectorAll(".mask-phone-js");
  let phonePlaceholder = document.querySelectorAll(
    ".mask-phone-placeholder-js"
  );
  let mailMask = document.querySelectorAll(".mask-mail-js");

  let maskOptionsPhone = {
    mask: "+{7}(000)000-00-00",
    lazy: false,
  };
  let maskOptionPhonePlaceholder = {
    mask: "+{7}(000)000-00-00",
  };
  let maskOptionsMail = {};
  mailMask.forEach(function (itemMail) {
    let mask = IMask(itemMail, maskOptionsMail);
  });
  phone.forEach(function (itemPhone) {
    let mask = IMask(itemPhone, maskOptionsPhone);
  });
  phonePlaceholder.forEach(function (itemPhonePlaceholder) {
    let mask = IMask(itemPhonePlaceholder, maskOptionPhonePlaceholder);
  });
}
