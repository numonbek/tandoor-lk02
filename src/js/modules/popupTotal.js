function popupTotal() {
  window.addEventListener("click", (event) => {
    if (event.target.closest(".popup-selection-master")) {
      const typeCloserr = document.querySelector(
        ".popup-selection-master--eight"
      );
      function container(container, block) {
        return container.querySelector(`.${block}`);
      }
      let firstAmount = container(
        typeCloserr,
        "popup-selection-master__right"
      ).querySelector("[data-popup-first]");
      let firstChildAmount = container(
        typeCloserr,
        "popup-selection-master__right"
      ).querySelector("[data-first-child]");
      let twoAmount = container(
        typeCloserr,
        "popup-selection-master__right"
      ).querySelector("[data-popup-two]");
      let twoChildAmount = container(
        typeCloserr,
        "popup-selection-master__right"
      ).querySelector("[data-two-child]");
      let thirdAmount = container(
        typeCloserr,
        "popup-selection-master__right"
      ).querySelector("[data-popup-third]");
      let thirdChildAmount = container(
        typeCloserr,
        "popup-selection-master__right"
      ).querySelector("[data-third-child]");

      const nTotal = container(
        typeCloserr,
        "popup-selection-master__price"
      ).querySelector("[data-popup-ntotal]");

      const nAmount = container(
        typeCloserr,
        "popup-selection-master__price"
      ).querySelector("[data-popup-namount]");

      nTotal.innerText =
        parseInt(firstAmount.innerText) +
        parseInt(twoAmount.innerText) +
        parseInt(thirdAmount.innerText);
      nAmount.innerText =
        parseInt(firstChildAmount.innerText) +
        parseInt(twoChildAmount.innerText) +
        parseInt(thirdChildAmount.innerText);

      console.log("nTotal", nTotal.innerText);
      console.log("nAmount", nAmount.innerText);
    }
  });
}
