function mgCategory() {
    try {
        console.log("mgCategory");

        const parent = document.querySelector(".profile-agent__wrapper-category");
        const parentButton = parent.querySelector("button");
        const searchCategory = document.querySelector('.mg-search-category');
        const formParent = searchCategory.closest('.popup-category-list__wrapper');
        const selsectLeftParent = document.querySelector('.popup-category-list__body-left');
        const selectLeftContent = selsectLeftParent.querySelector('.popup-category-list__list');
        let selectLeftInputs;
        /* selected items */
        const selectCategoryParent = document.querySelector('.popup-category-list__body-right');
        const selectedContent = selectCategoryParent.querySelector('.popup-category-list__list');
        /* mg-add-category__list */
        const categoryList = document.querySelector('.mg-add-categories__list');
        const urlCategory = 'http://localhost:9000/category';
        let selectListItems = [];
        let urlData = [];
        let deleteItem = [];
        const renderLocalCat = localStorage.getItem('categorys');
        categoryList.innerHTML = '';
        JSON.parse(renderLocalCat).map((item) => {
            categoryList.insertAdjacentHTML('beforeend', selectedItems(item));
        });
        parentButton.addEventListener('click', (e) => {
            preRenderLeft()
            localRender()
        })

        function delCat() {
            const deleteCategory = document.querySelectorAll('.mg-add-categories__close .Button__icon');
            deleteCategory.forEach((cat, index) => {
                cat.addEventListener('click', (e) => {
                    preRenderLeft()
                    cat.closest('.mg-add-categories__item').remove();
                    localRender(e.target.id)
                });
            });
        }
        delCat();
        formParent.addEventListener('submit', (e) => {
            e.preventDefault();
            deleteItem = [...selectListItems];
            localStorage.setItem('categorys', JSON.stringify(deleteItem));
            categoryList.innerHTML = '';
            selectListItems.map((item) => {
                categoryList.insertAdjacentHTML('beforeend', selectedItems(item));
            });
            delCat();
            $.fancybox.close();
        });
        searchCategory.addEventListener('input', (e) => {
            search(e.target.value);
        });
        sendRequest('GET', urlCategory)
            .then((data) => {
                urlData = [...data];
                localStorage.setItem('urldate', JSON.stringify(urlData));
                selectLeftContent.innerHTML = '';
                urlData.map((item) => {
                    selectLeftContent.insertAdjacentHTML('beforeend', renderCategory(item));
                });
            })
            .then(() => {
                preRenderLeft();
            });

        function processQ() {
            const selectRightInputs = selectedContent.querySelectorAll('input');
            selectRightInputs.forEach((value, index, array) => {
                value.addEventListener('input', (e) => {
                    selectListItems.map((item, index) =>
                        item.id == e.target.id ? selectListItems.splice(index, 1) : item,
                    );
                    let arrMassUrl = urlData.map((num) =>
                        num.id == e.target.id ? {...num, isChecked: 'unchecked' } : num,
                    );
                    urlData = [...arrMassUrl];
                    selectLeftContent.innerHTML = '';
                    urlData.map((item) => {
                        selectLeftContent.insertAdjacentHTML('beforeend', renderCategory(item));
                    });
                    selectedContent.innerText = '';
                    selectListItems.map((item) => {
                        selectedContent.insertAdjacentHTML('beforeend', renderCategory(item));
                    });
                    preRenderLeft();
                    countCategory()
                    return processQ();
                });
            });
        }

        function preRenderLeft() {
            selectLeftInputs = selectLeftContent.querySelectorAll('input');
            selectLeftInputs.forEach((value) => {
                value.addEventListener('input', (e) => {
                    selectedContent.innerText = '';
                    if (e.target.checked) {
                        selectListItems.push({
                            id: e.target.id,
                            img: e.target.dataset.imgSources,
                            name: e.target.name,
                            isChecked: e.target.checked ? 'checked' : 'unchecked',
                        });
                        const arr = urlData.map((obj) =>
                            obj.id == e.target.id ? {...obj, isChecked: 'checked' } : obj,
                        );
                        urlData = [...arr];
                    } else {
                        const arr = urlData.map((obj) =>
                            obj.id == e.target.id ? {...obj, isChecked: 'unchecked' } : obj,
                        );
                        urlData = [...arr];
                        selectListItems.map((item, index) =>
                            item.id == e.target.id ? selectListItems.splice(index, 1) : item,
                        );
                    }
                    selectListItems.map((item) => {
                        selectedContent.insertAdjacentHTML('beforeend', renderCategory(item));
                    });
                    countCategory()
                    processQ();
                });


            });
        }

        function search(q) {
            if (searchCategory.value == '') {
                selectLeftContent.innerHTML = '';
                urlData.map((item) => {
                    selectLeftContent.insertAdjacentHTML('beforeend', renderCategory(item));
                });
            } else {
                selectLeftContent.innerHTML = '';
                urlData
                    .filter((el) => el.name.toLowerCase().indexOf(q) > -1)
                    .map((item) => {
                        selectLeftContent.insertAdjacentHTML('beforeend', renderCategory(item));
                    });
            }
            preRenderLeft();
        }
        search();


        function localRender(idElem) {
            const catLocal = localStorage.getItem('categorys');
            const catUrlData = localStorage.getItem('urldate');
            let s = JSON.parse(catUrlData);
            if (idElem) {
                deleteItem = JSON.parse(catLocal).filter((el) => el.id !== idElem);
                localStorage.setItem('categorys', JSON.stringify(deleteItem));
            } else {
                deleteItem = JSON.parse(catLocal)
            }
            for (let i = 0; i < deleteItem.length; i++) {
                for (let j = 0; j < s.length; j++) {
                    deleteItem[i].id == s[j].id ? s[j] = {...s[j], isChecked: "checked" } : s[j]
                }
            }
            selectLeftContent.innerHTML = '';
            selectedContent.innerHTML = '';
            selectListItems = [...deleteItem]
            urlData = [...s]
            urlData.map((item) => {
                selectLeftContent.insertAdjacentHTML('beforeend', renderCategory(item));
            });
            selectListItems.map((item) => {
                selectedContent.insertAdjacentHTML('beforeend', renderCategory(item));
            });
            countCategory()
            preRenderLeft()
            processQ()
        }

        function countCategory() {
            const counterCategory = document.querySelector("[data-category-count]");
            counterCategory.innerText = selectListItems.length
        }
    } catch {
        console.log("ERROR mgCategory!!!!")
    }



}

function renderCategory(item) {
    return `<li class="popup-category-list__item">
    <div class="Checkbox Checkbox--default">
        <label class="Checkbox__label">
            <input type="checkbox" class="Checkbox__input" id="${item.id}" data-img-sources="${item.img}" name="${item.name}" ${item.isChecked}>
            <span class="Checkbox__inner-text">
                <span  class="Checkbox__text">${item.name}</span>
            </span>
        </label>
    </div>
</li>`;
}

function selectedItems(item) {
    return `<li class="mg-add-categories__item" id=${item.id}}>
    <div class="mg-add-categories__close">
        <span class="Button__icon" id=${item.id}>
            <svg class="Button__icon-svg">
                <use class="Button__icon-svg-use" xlink:href="#mg-pluss">
                </use>
            </svg>
        </span>
    </div>
    <div class="mg-add-categories__inner">
        <div class="photo">
            <span class="Button__icon">
                <svg class="Button__icon-svg">
                    <use class="Button__icon-svg-use" xlink:href="#${item.img}">
                    </use>
                </svg>
            </span>
        </div>
        <div class="text mg-colors--default">
            <span>${item.name}</span>
        </div>
    </div>
</li>`;
}

function sendRequest(method, url) {
    try {
        const delay = (ms) => {
            return new Promise((r) => {
                return setTimeout(() => {
                    r();
                }, ms);
            });
        };
        const headers = {
            'Content-type': 'application/json',
        };
        const requestBody = {
            method: method,
            headers: headers,
        };
        return delay().then(() => {
            return fetch(url, requestBody).then((response) => {
                if (response.ok) {
                    return response.json();
                }
                return response.json().then((error) => {
                    const e = new Error('Что-то пошло не так');
                    e.data = error;
                    throw e;
                });
            });
        });
    } catch (err) {
        console.log('Error SendRequest');
    }
}