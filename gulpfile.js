const { src, dest, parallel, series, watch } = require("gulp");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const uglify = require("gulp-uglify-es").default;
const del = require("del");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass");
const svgSprite = require("gulp-svg-sprite");
const fileInclude = require("gulp-file-include");
const sourcemaps = require("gulp-sourcemaps");
const rev = require("gulp-rev");
const revRewrite = require("gulp-rev-rewrite");
const revDel = require("gulp-rev-delete-original");
const htmlmin = require("gulp-htmlmin");
const gulpif = require("gulp-if");
const notify = require("gulp-notify");
const image = require("gulp-image");
const { readFileSync } = require("fs");
const concat = require("gulp-concat");
const babel = require("gulp-babel");
const rename = require("gulp-rename");

let isProd = false; // dev by default

const clean = () => {
  return del(["app/*"]);
};

//svg sprite
const svgSprites = () => {
  return src("./src/img/svg/**.svg")
    .pipe(
      svgSprite({
        mode: {
          stack: {
            sprite: "../sprite.svg",
          },
        },
      })
    )
    .pipe(dest("./app/img"))
    .pipe(concat("spriteSvg.html"))
    .pipe(dest("./src/layouts/spriteSvg"));
};

const stylesCss = () => {
  return src("./src/scss/css/*.css")
    .pipe(gulpif(!isProd, sourcemaps.init()))
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: false,
      })
    )
    .pipe(gulpif(isProd, cleanCSS({ level: 2 })))
    .pipe(gulpif(!isProd, sourcemaps.write(".")))
    .pipe(rename({ extname: ".min.css" }))
    .pipe(dest("./app/css/"))
    .pipe(browserSync.stream());
};

const styles = () => {
  return src("./src/scss/*.scss")
    .pipe(gulpif(!isProd, sourcemaps.init()))
    .pipe(sass().on("error", notify.onError()))
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: false,
      })
    )
    .pipe(gulpif(isProd, cleanCSS({ level: 2 })))
    .pipe(gulpif(!isProd, sourcemaps.write(".")))
    .pipe(rename({ extname: ".min.css" }))
    .pipe(dest("./app/css/"))
    .pipe(browserSync.stream());
};

const stylesBackend = () => {
  return src("./src/scss/*.scss")
    .pipe(sass().on("error", notify.onError()))
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: false,
      })
    )
    .pipe(rename({ extname: ".min.css" }))
    .pipe(dest("./app/css/"));
};

const scripts = () => {
  src("./src/js/libs-js/**.js")
    .pipe(
      babel({
        presets: ["@babel/preset-env"],
      })
    )
    .pipe(concat("libs-js.js"))
    .pipe(gulpif(isProd, uglify().on("error", notify.onError())))
    .pipe(rename({ extname: ".min.js" }))
    .pipe(dest("./app/js/"));
  return src(["./src/js/modules/**.js", "./src/js/main.js"])
    .pipe(gulpif(!isProd, sourcemaps.init()))
    .pipe(
      babel({
        presets: ["@babel/preset-env"],
      })
    )
    .pipe(concat("main.js"))
    .pipe(gulpif(isProd, uglify().on("error", notify.onError())))
    .pipe(gulpif(!isProd, sourcemaps.write(".")))
    .pipe(rename({ extname: ".min.js" }))
    .pipe(dest("./app/js"))
    .pipe(browserSync.stream());
};

const scriptsBackend = () => {
  src("./src/js/libs-js/**.js")
    .pipe(concat("libs-js.js"))
    .pipe(gulpif(isProd, uglify().on("error", notify.onError())))
    .pipe(rename({ extname: ".min.js" }))
    .pipe(dest("./app/js/"));
  return src(["./src/js/modules/**.js", "./src/js/main.js"])
    .pipe(concat("main.js"))
    .pipe(rename({ extname: ".min.js" }))
    .pipe(dest("./app/js"));
};

const resources = () => {
  return src("./src/resources/**").pipe(dest("./app"));
};

const images = () => {
  return src([
    "./src/img/**/*.jpg",
    "./src/img/**/*.png",
    "./src/img/**/*.jpeg",
    "./src/img/*.svg",
  ])
    .pipe(gulpif(isProd, image()))
    .pipe(dest("./app/img"));
};

const htmlInclude = () => {
  return src(["./src/*.html"])
    .pipe(
      fileInclude({
        prefix: "@",
        basepath: "@file",
      })
    )
    .pipe(dest("./app"))
    .pipe(browserSync.stream());
};

const watchFiles = () => {
  browserSync.init({
    server: {
      baseDir: "./app",
    },
  });

  watch("./src/**/*.scss", styles);
  watch("./src/js/**/*.js", scripts);
  watch("./src/components/**/*.html", htmlInclude);
  watch("./src/layouts/**/*.html", htmlInclude);
  watch("./src/*.html", htmlInclude);
  watch("./src/resources/**", resources);
  watch("./src/img/*.{jpg,jpeg,png,svg}", images);
  watch("./src/img/**/*.{jpg,jpeg,png}", images);
  watch("./src/img/svg/**.svg", svgSprites);
};

const cache = () => {
  return src("app/**/*.{css,js,svg,png,jpg,jpeg,woff2,woff,ttf}", {
    base: "app",
  })
    .pipe(rev())
    .pipe(dest("app"))
    .pipe(revDel())
    .pipe(rev.manifest("rev.json"))
    .pipe(dest("app"));
};

const rewrite = () => {
  const manifest = readFileSync("app/rev.json");

  return src("app/**/*.html")
    .pipe(
      revRewrite({
        manifest,
      })
    )
    .pipe(dest("app"));
};

// const buildHTML = () => {
//   return src("src/*.html").pipe(dest("app")).pipe(browserSync.stream());
// };

const htmlMinify = () => {
  return src("app/**/*.html")
    .pipe(
      htmlmin({
        collapseWhitespace: true,
      })
    )
    .pipe(dest("app"));
};

const toProd = (done) => {
  isProd = true;
  done();
};

exports.default = series(
  clean,
  htmlInclude,
  scripts,
  styles,
  stylesCss,
  resources,
  images,
  svgSprites,
  watchFiles
);

exports.build = series(
  toProd,
  clean,
  htmlInclude,
  scripts,
  styles,
  stylesCss,
  resources,
  images,
  svgSprites,
  htmlMinify
);

exports.cache = series(cache, rewrite);

exports.backend = series(
  toProd,
  clean,
  htmlInclude,
  scriptsBackend,
  stylesBackend,
  stylesCss,
  resources,
  images,
  svgSprites
);
